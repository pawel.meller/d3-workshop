import * as d3 from 'd3';

const seriesClasses = [
  'series series--purple',
  'series series--green',
  'series series--blue',
];

const pieSeriesClasses = [
  'series series--orange',
  'series series--light-orange',
  'series series--red',
  'series series--dark-red',
  'series series--dark-orange',
];

function renderStackArea (main, data) {
  main
    .selectAll('g.stack-area')
    .data([1])
    .enter()
    .append('g')
    .attr('style', 'transform: translate(400px, 50px)')
    .attr('class', 'stack-area');

  const stackGroup = main.select('g.stack-area');

  const stackGenerator = d3.stack()
    .keys(['apricots', 'blueberries', 'cherries']);

  const stackedSeriesData = stackGenerator(data);

  /**  TASK 1 SOLUTION: */
  const yScale = d3.scaleLinear().domain([0, 1500]).range([300, 0]);
  const stackedAreaGenerator = d3.area()
    .x((d, i) => i * 100)
    .y0(d => yScale(d[0]))
    .y1(d => yScale(d[1]));

  const stackAreaPaths = stackGroup
    .selectAll('path')
    .data(stackedSeriesData);

  stackAreaPaths
    .enter()
    .append('path')
    .merge(stackAreaPaths)
    .attr('class', (d, i) => seriesClasses[i])
    .attr('d', stackedAreaGenerator);

  stackAreaPaths.exit().remove();
}

function renderPie (main, data) {
  main
    .selectAll('g.pie')
    .data([1])
    .enter()
    .append('g')
    .attr('style', 'transform: translate(200px, 500px)')
    .attr('class', 'pie');

  const pieGroup = main.select('g.pie');

  const pieGenerator = d3.pie()
    .value(d => d.apricots)
    .sort(null);

  /**  TASK 2* SOLUTION: */
  const pieSeriesData = pieGenerator(data);

  const arcPieGenerator = d3.arc()
    .innerRadius(20)
    .outerRadius(100);

  const piePaths = pieGroup
    .selectAll('path')
    .data(pieSeriesData);

  piePaths
    .enter()
    .append('path')
    .attr('class', (d, i) => pieSeriesClasses[i])
    .merge(piePaths)
    .attr('d', arcPieGenerator);

  const pieTexts = pieGroup
    .selectAll('text')
    .data(pieSeriesData);

  pieTexts
    .enter()
    .append('text')
    .merge(pieGroup.selectAll('text'))
    .each(function (d) {
      const centroid = arcPieGenerator.centroid(d);
      d3.select(this)
        .attr('x', centroid[0])
        .attr('y', centroid[1])
        .attr('dy', '0.33em')
        .style('text-anchor', 'middle')
        .text(d.data.day);
    });
}
