import * as d3 from 'd3';
import { radialLineData } from '../data';
import { svgWidth, svgHeight } from '../config';

const seriesClasses = [
  'series series--orange',
  'series series--purple',
  'series series--red',
  'series series--green',
  'series series--blue',
];

function renderArea (main) {
  const lineData = [
    [0, 80],
    [10, 100],
    [20, 30],
    [30, 50],
    [40, 40],
    [50, 80],
  ];

  const areaGroup = main
    .append('g')
    .attr('class', 'area');

  /**  TASK 1 SOLUTION: */
  const xScale = d3.scaleLinear()
    .domain(d3.extent(lineData, d => d[0]))
    .range([0, svgWidth * 2 / 3]);

  const yScaleRev = d3.scaleLinear()
    .domain(d3.extent(lineData, d => d[1]))
    .range([svgHeight / 5, 0]);

  const areaGenerator = d3.area()
    .x(d => xScale(d[0]))
    .y0(yScaleRev(0))
    .y1(d => yScaleRev(d[1]))
    .curve(d3.curveCardinal);

  areaGroup
    .append('path')
    .attr('class', 'series series--red')
    .attr('d', areaGenerator(lineData));
}

function renderRadialLine (main) {
  const lineGroup = main
    .append('g')
    .style('transform', `translate(${svgWidth * 3 / 4}px, ${svgHeight / 4}px)`)
    .attr('class', 'radial-line');

  /**  TASK 2 SOLUTION: */
  const radialLineGenerator = d3.radialLine()
    .curve(d3.curveLinearClosed);

  lineGroup
    .append('path')
    .attr('class', 'star')
    .datum(radialLineData)
    .attr('d', radialLineGenerator);
};

function renderArc (main) {
  const arcData = [
    { name: 'One', startAngle: 0, endAngle: 1 },
    { name: 'Two', startAngle: 1, endAngle: 1.5 },
    { name: 'Three', startAngle: 1.5, endAngle: 2.1 },
  ];

  const arcGroup = main
    .append('g')
    .style('transform', `translate(${svgWidth / 3}px, ${svgHeight * 3 / 4}px)`)
    .attr('class', 'arc');

  /**  TASK 3 SOLUTION: */
  const arcGenerator = d3.arc()
    .innerRadius(20)
    .outerRadius(100);

  arcGroup
    .selectAll('path')
    .data(arcData)
    .enter()
    .append('path')
    .attr('class', (d, i) => seriesClasses[i])
    .attr('d', arcGenerator);

  /**  LIVE CODING: */
  // - add text for each arc in its center - centoroid
  arcGroup
    .selectAll('text') // - select all because we have many paths - many arcs
    .data(arcData)
    .enter()
    .append('text')
    .each(function (d) {
      const centroid = arcGenerator.centroid(d);
      d3.select(this)
        .attr('x', centroid[0])
        .attr('y', centroid[1])
        .attr('dy', '0.33em')
        .text(d.name);
    });
}
