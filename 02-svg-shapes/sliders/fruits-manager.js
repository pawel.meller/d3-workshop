import * as d3 from 'd3';
import './sliders.sass';
import { fruitsData } from '../data';

let onChange = null;
const currentData = [...fruitsData];
const labelsClasses = {
  'apricots': 'label label--purple',
  'blueberries': 'label label--green',
  'cherries': 'label label--blue',
};

function renderSlider (element, value, dataKey, dataIndex) {
  const sliderElement = element.append('div')
    .attr('class', 'slider');

  sliderElement.append('input')
    .attr('type', 'range')
    .attr('min', '0')
    .attr('max', '500')
    .attr('step', '1')
    .property('value', value)
    .on('input', function () {
      const val = d3.select(this).property('value');
      currentData[dataIndex][dataKey] = val;

      sliderElement.select('label').text(val);

      if (onChange) {
        onChange(currentData);
      }
    });

  sliderElement.append('label')
    .attr('class', () => labelsClasses[dataKey])
    .text(value);
}

export function renderSliders (element, changedCallback) {
  onChange = changedCallback;

  fruitsData.forEach((fruitsForDay, index) => {
    const dayElement = element.append('div')
      .attr('class', 'day');

    dayElement.append('label')
      .text(fruitsForDay.day);


    renderSlider(dayElement, fruitsForDay.apricots, 'apricots', index);
    renderSlider(dayElement, fruitsForDay.blueberries, 'blueberries', index);
    renderSlider(dayElement, fruitsForDay.cherries, 'cherries', index);
  });
}
