import * as d3 from 'd3';
import './lesson02.sass';
import { fruitsData } from './data';
import { svgWidth, svgHeight } from './config';
import { renderSliders } from './sliders/fruits-manager';
import { renderStackBars, renderStackArea, renderPie } from './lessonModules/stacked-shapes-render';

const lessonTitle = 'D3.js - Lesson 2: d3 stacked charts';

/**
  Render main DOM content.
*/
const main = d3.select('body')
  .append('div')
  .attr('class', 'main');

main
  .append('h1')
  .classed('title', true)
  .text(lessonTitle);

main
  .append('div')
  .attr('class', 'main__stacked-shapes')
  .append('svg')
  .attr('class', 'stacked-shapes')
  .attr('width', `${svgWidth}px`)
  .attr('height', `${svgHeight}px`);

const slidersWrapper = main
  .append('div')
  .attr('class', 'sliders');

/**
  Render dynamic stacked charts (shapes).

  - using d3.shapes() to dynamically draw stacked charts
  - stacked d3.shapes() (area, arc, stack, centeroid, pie)
*/
const renderStackShapes = function (data) {
  const shapesElement = main.select('.stacked-shapes');

  //renderStackBars(shapesElement, data);
  renderStackArea(shapesElement, data);
  renderPie(shapesElement, data);
};

// Render sliders that will trigger re-render with new data.
renderSliders(slidersWrapper, (newFruitsData) => {
  renderStackShapes(newFruitsData);
});

renderStackShapes(fruitsData);

