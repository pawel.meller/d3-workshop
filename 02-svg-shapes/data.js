export const radialLineData = [
  [0, 80],
  [2 * Math.PI / 10, 40],
  [2 * Math.PI / 5, 80],
  [2 * Math.PI * 3 / 10, 40],
  [2 * Math.PI * 2 / 5, 80],
  [2 * Math.PI * 5 / 10, 40],
  [2 * Math.PI * 3 / 5, 80],
  [2 * Math.PI * 7 / 10, 40],
  [2 * Math.PI * 4 / 5, 80],
  [2 * Math.PI * 9 / 10, 40],
  // [2 * Math.PI, 80],
];

export const fruitsData = [
  {day: 'Mon', apricots: 120, blueberries: 180, cherries: 150},
  {day: 'Tue', apricots: 60, blueberries: 185, cherries: 105},
  {day: 'Wed', apricots: 100, blueberries: 215, cherries: 110},
  {day: 'Thu', apricots: 80, blueberries: 230, cherries: 55},
  {day: 'Fri', apricots: 120, blueberries: 240, cherries: 25},
];
