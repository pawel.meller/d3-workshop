import * as d3 from 'd3';
import { svgWidth, svgHeight } from '../config';
export { renderStackBars } from './stacked-bars-render';

const seriesClasses = [
  'series series--purple',
  'series series--green',
  'series series--blue',
];

const pieSeriesClasses = [
  'series series--orange',
  'series series--light-orange',
  'series series--red',
  'series series--dark-red',
  'series series--dark-orange',
];

/* Data structure
Fruits data:
  [
    {day: 'Mon', apricots: 120, blueberries: 180, cherries: 150},
    {day: 'Tue', apricots: 60, blueberries: 185, cherries: 105},
    {day: 'Wed', apricots: 100, blueberries: 215, cherries: 110},
    {day: 'Thu', apricots: 80, blueberries: 230, cherries: 55},
    {day: 'Fri', apricots: 120, blueberries: 240, cherries: 25},
  ]
* */



/* ----------- TASKS: ---------------

  1. Implement renderStackArea function to draw stack-area "chart" of all fruits data for each day.
     This function will be called dynamically so it must implement enter/update/exit scenarios.

  2*. Draw pie chart for apricots for each day. Add labels (using centeroid) for each pie arc;
     This function will be called dynamically so it must implement enter/update/exit scenarios.

  Time: 20 min

  -------------------------------- */

//  * main - d3 element to which new data-driven elements should be appended
//  * data - data to be used to draw stacked charts (data structure shown above)
export function renderStackArea (main, data) {
  main
    .selectAll('g.stack-area')
    .data([1])
    .enter()
    .append('g')
    .attr('style', 'transform: translate(400px, 10px)')
    .attr('class', 'stack-area');

  const stackGroup = main.select('g.stack-area');

  const stackGenerator = d3.stack()
    .keys(['apricots', 'blueberries', 'cherries']);

  const stackedSeriesData = stackGenerator(data);
  // Sample d3.stack()(data) output:
  // [
  //   [ [0, 120],   [0, 60],   [0, 100],    [0, 80],    [0, 120] ],   // Apricots
  //   [ [120, 300], [60, 245], [100, 315],  [80, 310],  [120, 360] ], // Blueberries
  //   [ [300, 450], [245, 350], [315, 425], [310, 365], [360, 385] ]  // Cherries
  // ]


  /*
        ------ YOUR CODE HERE (TASK #1) ------

        Hint: stackGenerator(data) will only generate transformed data - not a ready to use path
        Hint: Combine d3.stack()(data) output with d3.area() to transform it to path.
        Hint: Use yScale to invert y orientation for area.



        const yScale = ...

        const stackedAreaGenerator = d3.area()...

        const stackAreaPaths = stackGroup
          .selectAll(...)
          .data(...);

        stackAreaPaths
          .enter()
          ...
          .merge(...)
          ...

  */
}

//  * main - d3 element to which new data-driven elements should be appended
//  * data - data to be used to draw stacked charts (data structure shown above)
export function renderPie (main, data) {
  main
    .selectAll('g.pie')
    .data([1])
    .enter()
    .append('g')
    .attr('style', 'transform: translate(200px, 500px)')
    .attr('class', 'pie');

  const pieGroup = main.select('g.pie');

  const pieGenerator = d3.pie()
    .value(d => d.apricots) // value getter
    .sort(null); // sort must be set - null = in the order that data was provided

  // Sample d3.pie()(data) output:
  // [
  //  {data: {day: "Mon", apricots: 120, blueberries: 180, cherries: 150}, index: 0, value: 120, startAngle: 0, endAngle: 1.570}
  //  {data: {day: "Tue", apricots: 60, blueberries: 185, cherries: 105}, index: 1, value: 60, startAngle: 1.570, endAngle: 2.356}
  //  ...
  // ]

  /*
        ------ YOUR CODE HERE (TASK #2) ------

        Hint: pieGenerator(data) will only generate transformed data - not a ready to use path - similar as for d3.stack()
        Hint: Combine d3.pie()(data) output with d3.arc() to transform it to path.



        const pieSeriesData = pieGenerator(data);

        const arcGenerator = d3.arc()...

        const piePaths = pieGroup
          .selectAll(...)
          .data(...);

        piePaths
          .enter()
          ...
          .merge(...)
          ...

  */
}
