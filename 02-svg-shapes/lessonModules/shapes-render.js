import * as d3 from 'd3';
import { radialLineData } from '../data';
import { svgWidth, svgHeight } from '../config';

const seriesClasses = [
  'series series--orange',
  'series series--purple',
  'series series--red',
];

/* ----------- TASKS: ---------------

  1. Implement renderArea function to draw line area "chart".
    Use one of seriesClasses to set css class or use plain style fill attribute to cover the area.

  2. Implement renderRadialLine function to draw star.
    Use imported radialLineData or create yor own

  3*. Implement renderArc function to draw arcs.
     Use seriesClasses to get css class for arc (by index).

  Time: 15 min

  -------------------------------- */



//  * main - d3 element to which new data-driven elements should be appended
export function renderLineExample (main) {
  const lineData = [
    [0, 80],
    [10, 100],
    [20, 30],
    [30, 50],
    [40, 40],
    [50, 80],
  ];

  // Use scales to "map" domain values to range values ("pixels" within svg area in which shape should be drawn)
  // const xScale = d3.scaleLinear()
  //   .domain(d3.extent(lineData, d => d[0]))
  //   .range([0, svgWidth * 2 / 3]);
  //
  // const yScale = d3.scaleLinear()
  //   .domain(d3.extent(lineData, d => d[1]))
  //   .range([svgHeight / 5, 0]);

  const lineGroup = main.append('g')
    .style('transform', `translate(${svgWidth / 10}px, 20px)`)
    .attr('class', 'line-example');

  // d3.line()(data) returns ready to use path.d value
  const lineGenerator = d3.line()
    // If data items are simple 2-element arrays of coordinates ([x, y]), you can skip x, y functions
    .x(d => (d[0]))
    .y(d => (d[1]))
    // You can set different curves for line
    .curve(d3.curveCardinal);

  lineGroup
    .append('path')
    .datum(lineData)
    .attr('d', lineGenerator);

  // Alternative ways to set data:
  // - remove datum(lineData). Instead call attr('d', lineGenerator(lineData)). __data will not be set in this case.
  // - datum(lineData) -> data([lineData]) This will create "enter/exit, update" selections. See diff in console.
}

//  * main - d3 element to which new data-driven elements should be appended
export function renderArea (main) {
  const lineData = [
    [0, 80],
    [10, 100],
    [20, 30],
    [30, 50],
    [40, 40],
    [50, 80],
  ];

  const areaGroup = main.append('g')
    .style('transform', `translate(${svgWidth / 10}px, 200px)`)
    .attr('class', 'area');

  /*
        ------ YOUR CODE HERE (TASK #1) ------

        Hint: Use d3.area() - works like d3.line() but set y0 and y1 functions (y0 is a baseline - can be static val)
        Hint: Use linear scales to draw shape. Use yScale to invert y orientation for area.

        const xScale = d3.scaleLinear()...
        const yScale = d3.scaleLinear()...
        const areaGenerator = ...

        areaGroup
          .append('path')...
  */
}

//  * main - d3 element to which new data-driven elements should be appended
export function renderRadialLine (main) {
  const lineGroup = main.append('g')
    .style('transform', `translate(${svgWidth / 4}px, 520px)`)
    .attr('class', 'radial-line');

  /*
        ------ YOUR CODE HERE (TASK #2) ------

        Hint: Use d3.radialLine() which works like d3.line() but it accepts [angle, radius] instead of [x, y].
        Hint: Use curveLinearClosed / curveBasisClosed.

        const lineGenerator = ...

        lineGroup
          .append('path')...

  */
}

//  * main - d3 element to which new data-driven elements should be appended
export function renderArc (main) {
  const arcData = [
    { name: 'One', startAngle: 0, endAngle: 1 },
    { name: 'Two', startAngle: 1, endAngle: 1.9 },
    { name: 'Three', startAngle: 1.9, endAngle: 4 },
  ];

  const arcGroup = main.append('g')
    .style('transform', `translate(${svgWidth / 2}px, 520px)`)
    .attr('class', 'arc');

  /*
        ------ YOUR CODE HERE (TASK #3) ------

        Hint: Use d3.arc(). It has innerRadius(x) and outerRadius(y) setters (must be set).
        Hint: There are more than one arcs to be drawn so instead of datum use selectAll + data with enter selection.

        const arcGenerator = ...

        arcGroup
          .selectAll('path')
          .data(...)...
  */
}





// READ MORE:
//
// D3 SHAPES: https://github.com/d3/d3-shape
//            http://d3indepth.com/shapes/
//
