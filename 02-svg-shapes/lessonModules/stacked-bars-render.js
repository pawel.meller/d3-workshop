import * as d3 from 'd3';
import { svgWidth, svgHeight } from '../config';

const seriesClasses = [
  'series series--purple',
  'series series--green',
  'series series--blue',
];

/* Data structure
Fruits data:
  [
    {day: 'Mon', apricots: 120, blueberries: 180, cherries: 150},
    {day: 'Tue', apricots: 60, blueberries: 185, cherries: 105},
    {day: 'Wed', apricots: 100, blueberries: 215, cherries: 110},
    {day: 'Thu', apricots: 80, blueberries: 230, cherries: 55},
    {day: 'Fri', apricots: 120, blueberries: 240, cherries: 25},
  ]
* */



//  * main - d3 element to which new data-driven elements should be appended
//  * data - data to be used to draw stacked charts (data structure shown above)
export function renderStackBars (main, data) {
  // This method will be called many times, so it must be "smart" add or update
  main
    .selectAll('g.stack-bars')
    .data([1])
    .enter()
    .append('g')
    .attr('style', 'transform: translate(50px, 10px)')
    .attr('class', 'stack-bars');

  const stackGroup = main.select('g.stack-bars');

  // d3.stack()(data) returns transformed data - not a ready to use d path.
  // It can be used than as data source for another shape or for custom rendering methods
  const stackGenerator = d3.stack()
    .keys(['apricots', 'blueberries', 'cherries']);

  const stackedSeriesData = stackGenerator(data);
  // Sample d3.stack()(data) output:
  // [
  //   [ [0, 120],   [0, 60],   [0, 100],    [0, 80],    [0, 120] ],   // Apricots
  //   [ [120, 300], [60, 245], [100, 315],  [80, 310],  [120, 360] ], // Blueberries
  //   [ [300, 450], [245, 350], [315, 425], [310, 365], [360, 385] ]  // Cherries
  // ]

  const yScale = d3.scaleLinear().domain([0, 1500]).range([0, 300]);
  const yScaleRev = d3.scaleLinear().domain([0, 1500]).range([300, 0]);

  const series = stackGroup
    .selectAll('g.series')
    .data(stackedSeriesData);

  const newSeries = series
    .enter()
    .append('g')
    .attr('class', (d, i) => seriesClasses[i]);

  const rects = newSeries.merge(series)
    .selectAll('rect')
    .data(d => d);

  rects
    .enter()
    .append('rect')
    .merge(rects)
    .attr('height', d => yScale(d[1] - d[0]))
    .attr('width', d => 40)
    .attr('y', d => yScaleRev(d[1]))
    .attr('x', (d, i) => i * 50);

  rects.exit().remove();
}
