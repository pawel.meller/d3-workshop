import * as d3 from 'd3';
import './lesson02.sass';
import { svgWidth, svgHeight } from './config';
import { renderLineExample, renderRadialLine, renderArea, renderArc } from './lessonModules/shapes-render';

const lessonTitle = 'D3.js - Lesson 2: d3 shapes';

/**
  Render main DOM content.
*/
const main = d3.select('body')
  .append('div')
  .attr('class', 'main');

main
  .append('h1')
  .classed('title', true)
  .text(lessonTitle);

main
  .append('div')
  .attr('class', 'main__shapes')
  .append('svg')
  .attr('class', 'shapes')
  .attr('width', `${svgWidth}px`)
  .attr('height', `${svgHeight}px`);

/**
  Render shapes.

  - using d3.shapes() to simplify drawing svg paths ((line, radialLine, area, arc))
  - using d3.scale() to map data domain to svg pixels range.
*/
const renderShapes = function () {
  const shapesElement = main.select('.shapes');

  //renderLineExample(shapesElement);
  renderRadialLine(shapesElement);
  renderArea(shapesElement);
  renderArc(shapesElement);
};

renderShapes();




