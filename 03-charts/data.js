import axios from 'axios';
import _ from 'lodash';
import { reactRepoUrlPath, d3RepoUrlPath, contributorsUrlPath, commitsUrlPath, codeUrlPath } from './config';

import reactContributorsJson from './static/react/contributors.json';
import reactCommitsJson from './static/react/commit_activity.json';
import reactCodeJson from './static/react/code_frequency.json';

import d3ContributorsJson from './static/d3/contributors.json';
import d3CommitsJson from './static/d3/commit_activity.json';
import d3CodeJson from './static/d3/code_frequency.json';

const staticData = {
  [`${reactRepoUrlPath}/${contributorsUrlPath}`]: reactContributorsJson,
  [`${reactRepoUrlPath}/${commitsUrlPath}`]: reactCommitsJson,
  [`${reactRepoUrlPath}/${codeUrlPath}`]: reactCodeJson,
  [`${d3RepoUrlPath}/${contributorsUrlPath}`]: d3ContributorsJson,
  [`${d3RepoUrlPath}/${commitsUrlPath}`]: d3CommitsJson,
  [`${d3RepoUrlPath}/${codeUrlPath}`]: d3CodeJson,
};

const loadData = (url, useStaticData) => {
  if (useStaticData) {
    return Promise.resolve(staticData[url]);
  }

  return new Promise((resolve, reject) =>
    axios.get(url)
      .then(response => resolve(response.data))
      .catch(error => reject(error))
  );
};

export const loadRankingData = ({url, useStaticData}) => {
  return loadData(url, useStaticData)
    .then((data) => {
      return _.chain(data)
        .sortBy(d => d.total * -1)
        .slice(0, 5)
        .map((d) => ({ name: d.author.login, value: d.total }))
        .value();
    });
};

export const loadCommitsData = ({url, useStaticData}) => {
  const now = new Date();
  now.setFullYear(now.getFullYear() - 1);

  return loadData(url, useStaticData)
    .then((data) => {
      return _.chain(data)
        .filter(d => new Date(d.week * 1000) >= now)
        .map(d => ({ time: new Date(d.week * 1000), value: d.total }))
        .value();
    });
};

export const loadCodeChangesData = ({url, useStaticData}) => {
  const now = new Date();
  now.setFullYear(now.getFullYear() - 1);

  return loadData(url, useStaticData)
    .then((data) => {
      return _.chain(data)
        .filter(d => new Date(d[0] * 1000) >= now)
        .map(d => ({ time: new Date(d[0] * 1000), value: d[1], value2: d[2] * -1 }))
        .value();
    });
};
