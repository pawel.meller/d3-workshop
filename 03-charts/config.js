export const svgWidth = 600;
export const svgHeight = 300;

export const reactRepoUrlPath = 'https://api.github.com/repos/facebook/react';
export const d3RepoUrlPath = 'https://api.github.com/repos/d3/d3';
export const contributorsUrlPath = 'stats/contributors';
export const commitsUrlPath = 'stats/commit_activity';
export const codeUrlPath = 'stats/code_frequency';
