import './lesson03.sass';
import { reactRepoUrlPath, d3RepoUrlPath, contributorsUrlPath, commitsUrlPath, codeUrlPath } from './config';
import { BarRankingChart } from './lessonModules/barRankingChart';
import { BarTimeChart } from './lessonModules/barTimeChart';
import { LineChart } from './lessonModules/lineChart';
import { ScatterChart } from './lessonModules/scatterChart';
import { loadRankingData, loadCommitsData, loadCodeChangesData } from './data';
import * as d3 from 'd3';

const lessonTitle = 'D3.js - Lesson 3: Charts';

/**
 Render main DOM content.
 */
const main = d3.select('body')
  .append('div')
  .attr('class', 'main');

main
  .append('h1')
  .classed('title', true)
  .text(lessonTitle);

const links = main.append('div')
  .attr('class', 'links');

links.append('button')
  .attr('id', 'reactButton')
  .text('React')
  .on('click', () => {
    loadCharts({ baseUrl: reactRepoUrlPath, useStaticData: true });
  });

links.append('button')
  .attr('id', 'd3Button')
  .text('d3')
  .on('click', () => {
    loadCharts({ baseUrl: d3RepoUrlPath, useStaticData: true });
  });

const repoForm = main
  .append('div')
  .attr('class', 'load-form');

repoForm.append('input')
  .attr('id', 'sourceUrl')
  .property('value', reactRepoUrlPath);

repoForm.append('button')
  .attr('id', 'btnLoad')
  .text('Load')
  .on('click', () => {
    const baseUrl = d3.select('#sourceUrl').property('value');
    loadCharts({ baseUrl, useStaticData: false });
  });

const container = main;

/**
 Create dashboard with charts.

 Rendering X and Y axes of different types (time, linear value, "band").
 Adding zoom behaviour.

 - using d3.axis() to create and draw axes
 - using different types of d3.scale()
 - using d3.zoom() to provide zoom behaviour

 */

const rankingChart = new BarRankingChart(container);
const lineChart1 = new LineChart(container);
const lineChart2 = new LineChart(container);
const scatterChart = new ScatterChart(container);
const barTimeChart = new BarTimeChart(container);

function loadCharts ({ baseUrl, useStaticData }) {
  loadRankingData({ url: `${baseUrl}/${contributorsUrlPath}`, useStaticData })
    .then(data => {
      rankingChart
        .setScales(data)
        .render(data);
    });

  Promise.all([
    loadCommitsData({ url: `${baseUrl}/${commitsUrlPath}`, useStaticData }),
    loadCodeChangesData({ url: `${baseUrl}/${codeUrlPath}`, useStaticData }),
  ])
    .then(values => {
      lineChart1
        .setScales(values[0])
        .render(values[0])
        .addZoomBehaviour();

      lineChart2
        .setScales(values[1])
        .render(values[1])
        .addZoomBehaviour();

      scatterChart
        .setScales(values[1])
        .render(values[1])
        .addZoomBehaviour();

      // barTimeChart
      //   .setScales(values[0])
      //   .render(values[0])
      //   .addBrushBehaviour(function (newDomain) {
      //     lineChart1.updateXDomain(newDomain);
      //     lineChart2.updateXDomain(newDomain);
      //
      //     lineChart1.render(values[0]);
      //     lineChart2.render(values[1]);
      //     scatterChart
      //       .render(values[1].filter(d => d.time >= newDomain[0] && d.time <= newDomain[1]));
      //   });
    });
}





// READ MORE:
// SCALES:      https://github.com/d3/d3/blob/master/API.md#scales-d3-scale
// AXES:        https://github.com/d3/d3-axis
// TIME FORMAT: https://github.com/d3/d3/blob/master/API.md#time-formats-d3-time-format
// ZOOM:        https://github.com/d3/d3/blob/master/API.md#zooming-d3-zoom

// AXES EXAMPLE:                    https://bl.ocks.org/mbostock/3371592
// CHART WITH ZOOMING EXAMPLE:      https://bl.ocks.org/mbostock/db6b4335bf1662b413e7968910104f0f
// CHART WITH WITH BRUSH EXAMPLE:   https://bl.ocks.org/mbostock/34f08d5e11952a80609169b7917d4172
//

