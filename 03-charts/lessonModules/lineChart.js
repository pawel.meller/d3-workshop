import * as d3 from 'd3';
import { BaseChart } from './baseChart';

/* Data structure:
  [
    { time: Date Object, value: Number },
    { time: Date Object, value: Number },
    { time: Date Object, value: Number },
    ...
  ]
* */



/* ----------- TASKS: ---------------

  1. Implement setScales  and render functions to draw line chart.
    Use d3.scaleTime() for x scale.
    Use d3.scaleLinear() for y scale.
    This function will be called dynamically so it must implement enter/update/exit scenarios.

  2. Implement renderXAxis and renderYAxis functions to add axes to the chart.

  3*. Add zoom behaviour to the chart
    Add invisible (fill:none) rect to cover entire chart area (it will be zooming event "catcher"). Set its
     'pointer-events' to 'all' to catch all events.
    Use d3.zoom() to create zoom behaviour.

  Time: 20 min

  -------------------------------- */



export class LineChart extends BaseChart {
  constructor (container) {
    super(container, 'line-chart');
  }

  //  data - data to be used to draw line chart (data structure shown above)
  setScales (data) {
    const { width, height } = this;

    /*
        ------ YOUR CODE HERE (TASK #1) ------

        Note: Store both scales in "this", as they will be needed in other class methods.


        this.xScale = d3.scaleTime() ...
        this.yScale = d3.scaleLinear() ...

    */

    return this;
  }

  renderAxes () {
    const { main, xScale, yScale, width, height } = this;

    const xAxisElement = main
      .select('.xAxis');

    const yAxisElement = main
      .select('.yAxis');
    /*
        ------ YOUR CODE HERE (TASK #2) ------

        Note: Store both scales in "this", as they will be needed in other class methods.

        Hint:
          axis = d3.axisBottom(xScale)              to create axis.
          axis.tickFormat(d3.timeFormat('%Y-%m'))   to set axis ticks format.
          element.call(axis)                        to render axis.
          See: https://bl.ocks.org/mbostock/3371592 for help.

        Hint: Bottom axis by default will be drawn at the top of the chart. Use style transform to move axis element
          down by the chart height. Chart height is available as: this.height


        this.xAxis = d3.axisBottom(xScale);
        xAxisElement...

        this.yAxis = d3.axisLeft(yScale);
        yAxisElement...

    */

    return this;
  }

  //  data - data to be used to draw line chart (data structure shown above)
  render (data) {
    this.renderAxes();

    const { main, xScale, yScale } = this;

    const seriesElement = main
      .select('.series');

    /*
        ------ YOUR CODE HERE (TASK #1) ------

        Note: Draw chart within seriesElement provided above.

        Hint: Use lineGenerator. Then use class .line for the path.
        Hint: Even though there is only one path to be drawn, use selectAll + data to handle enter/update/exit.
              Data must be "wrapped" in this case as one element array ( [data] )


        const lineGenerator = d3.line()
          .x( ...
          .y( ...

        const path = seriesElement
          .selectAll(...)
          .data(...);

        path
          .enter()
          ...
          .merge(...)
          ...

    */

    return this;
  }

  addZoomBehaviour () {
    const { main, width, height } = this;
    const { xAxis, yAxis, xScale, yScale } = this;

    /*
        ------ YOUR CODE HERE (TASK #3) ------

        Hint:
          zoom = d3.zoom()                  to create zoom behaviour.
          zoom.scaleExtent([min, max])      to set tha scaling range.
          zoom.translateExtent([min, max])  to set tha translate range (zoomed element can be moved via drag&drop).
          element.call(zoom)                to apply zoom behaviour to element.
          .on('zoom', () => {})             to set callback function for zooming event - chart will not "transform" by
                                            itself but you must do it using d3.event.transform object (global)
              - use it "as is" to set transform attribute on chart series element
              - use d3.event.transform.rescaleX and d3.event.transform.rescaleY to apply new scales on axes
                  like: newXScale = d3.event.transform.rescaleX(xScale)
                        axisElement.call(xAxis.scale(newXScale))

          See https://bl.ocks.org/mbostock/db6b4335bf1662b413e7968910104f0f for help



        const zoomElement = main
          .append('rect')
          ...

        const zoom = d3.zoom()
          .scaleExtent(...)
          .translateExtent(...)
          .on('zoom', () => {
            main
              .select('.series')
              .attr('transform', ...);

            main
              .select('.xAxis')
              .call(xAxis.scale(d3.event.transform.rescaleX(xScale)));

            main
              .select('.yAxis')
              ...

          });

        zoomElement
          .call...

    */

    return this;
  }

  updateXDomain (domain) {
    // this.xScale.domain(domain);
  }
}
