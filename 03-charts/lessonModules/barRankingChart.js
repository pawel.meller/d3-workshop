import * as d3 from 'd3';
import { BaseChart } from './baseChart';

/* Data structure:
  [
    { id: Number, name: String, value: Number },
    { id: Number, name: String, value: Number },
    { id: Number, name: String, value: Number },
    ...
  ]
* */



/* ----------- TASKS: ---------------

  1. Implement setScales  and render functions to draw "ranking" bar chart.
    Use d3.scaleLinear() for x scale.
    Use d3.scaleBand() for y scale.
    This function will be called dynamically so it must implement enter/update/exit scenarios.

  2. Implement renderXAxis and renderYAxis functions to add axes to the chart.
  2*. Customize you axes (tick format etc)

  Time: 20 min

  -------------------------------- */



export class BarRankingChart extends BaseChart {
  constructor (container) {
    super(container, 'bar-chart');
  }

  //  data - data to be used to draw line chart (data structure shown above)
  setScales (data) {
    const { width, height } = this;

    /*
        ------ YOUR CODE HERE (TASK #1) ------

        Note: Store both scales in "this", as they will be needed in other class methods.

        Hint: d3.scaleBand() is discrete scale so you provide domain as an array of all bands (in this case all names).
        Hint: Use .paddingInner(...) on this scaleBand to se the distance between "bands".


        this.xScale = d3.scaleLinear() ...
        this.yScale = d3.scaleBand() ...

    */

    return this;
  }

  renderAxes () {
    const { main, xScale, yScale, width, height } = this;

    const xAxisElement = main
      .select('.xAxis');

    const yAxisElement = main
      .select('.yAxis');
    /*
        ------ YOUR CODE HERE (TASK #2) ------

        Note: Store both scales in "this", as they will be needed in other class methods.

        Hint:
          axis = d3.axisBottom(xScale)  to create axis.
          element.call(axis)            to render axis.
          See: https://bl.ocks.org/mbostock/3371592 for help.

        Hint: Bottom axis by default will be drawn at the top of the chart. Use style transform to move axis element
          down by the chart height. Chart height is available as: this.height


        this.xAxis = d3.axisBottom(xScale);
        xAxisElement...

        this.yAxis = d3.axisLeft(yScale);
        yAxisElement...

    */

    return this;
  }

  //  data - data to be used to draw line chart (data structure shown above)
  render (data) {
    this.renderAxes();

    const { main, xScale, yScale } = this;

    const seriesElement = main
      .select('.series');

    /*
        ------ YOUR CODE HERE (TASK #1) ------

        Note: Draw chart within seriesElement provided above.

        Hint: Use svg rect to draw bars. Then use class .bar for them. You can use your code from Lesson 1.
        Hint: Use id as data key.


        const bars = seriesElement
          .selectAll(...)
          .data(...);

        bars
          .enter()
          ...
          .merge(...)
          ...

    */

    return this;
  }
}
