import * as d3 from 'd3';
import { BaseChart } from './baseChart';

/* Data structure:
  [
    { time: Date Object, value: Number },
    { time: Date Object, value: Number },
    { time: Date Object, value: Number },
    ...
  ]
* */

export class BarTimeChart extends BaseChart {
  constructor (container) {
    super(container, 'time-chart');

    this.main
      .append('g')
      .attr('class', 'bar-time-brush');
  }

  //  data - data to be used to draw line chart (data structure shown above)
  setScales (data) {
    this.xScale = d3.scaleTime()
      .domain(d3.extent(data, d => d.time))
      .range([0, this.width]);

    this.yScale = d3.scaleLinear()
      .domain(d3.extent(data, d => d.value))
      .range([this.height, 0]);

    return this;
  }

  renderAxes (xScale, yScale) {
    const xAxisElement = this.main
      .select('.xAxis');

    const yAxisElement = this.main
      .select('.yAxis');

    this.xAxis = d3.axisBottom(xScale);
    xAxisElement
      .attr('transform', () => `translate(0, ${this.height})`)
      .call(this.xAxis);

    this.yAxis = d3.axisLeft(yScale);
    yAxisElement
      .call(this.yAxis);

    return this;
  }

  //  data - data to be used to draw line chart (data structure shown above)
  render (data) {
    this.renderAxes(this.xScale, this.yScale);

    const bars = this.main
      .select('.series')
      .selectAll('rect')
      .data(data, d => d.time.getTime());

    bars.enter()
      .append('rect')
      .attr('class', 'bar')
      .merge(bars)
      .attr('width', '5px')
      .attr('x', d => this.xScale(d.time))
      .attr('y', d => this.yScale(d.value))
      .transition()
      .duration(1000)
      .attr('height', d => this.height - this.yScale(d.value));

    bars.exit().remove();

    return this;
  }

  addBrushBehaviour (onBrushed) {
    const brush = d3.brushX()
      .extent([[0, this.height / 2], [this.width, this.height]])
      .on('brush end', () => {
        const range = d3.event.selection || this.xScale.range();
        const newDomain = range.map(this.xScale.invert, this.xScale);

        onBrushed(newDomain);
      });

    this.main.select('.bar-time-brush')
      .call(brush)
      .call(brush.move, this.xScale.range());

    return this;
  }
}
