import { svgWidth, svgHeight } from '../config';

export class BaseChart {
  constructor (container, mainElementClass) {
    this.margin = {top: 20, right: 0, bottom: 50, left: 70};
    this.width = svgWidth - this.margin.left - this.margin.right;
    this.height = svgHeight - this.margin.top - this.margin.bottom;

    this.svg = container
      .append('svg')
      .attr('width', `${svgWidth}px`)
      .attr('height', `${svgHeight}px`);

    this.main = this.svg
      .append('g')
      .attr('class', mainElementClass)
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

    this.svg.append('defs').append('svg:clipPath')
      .attr('id', `clip-${mainElementClass}`)
      .append('rect')
      .attr('width', this.width)
      .attr('height', this.height);

    this.main
      .append('g')
      .attr('class', 'xAxis');

    this.main
      .append('g')
      .attr('class', 'yAxis');

    this.main
      .append('g')
      .attr('class', 'series-wrapper')
      .attr('clip-path', `url(#clip-${mainElementClass})`)
      .append('g')
      .attr('class', 'series');
  }
}

