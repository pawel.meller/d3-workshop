import * as d3 from 'd3';

class BaseChart {
}

export class BarRankingChart extends BaseChart {
  constructor (container) {
    super(container, 'bar-chart');
  }

  setScales (data) {
    const { width, height } = this;

    this.xScale = d3.scaleLinear()
      .domain([0, d3.max(data, d => d.value)])
      .range([0, width]);

    this.yScale = d3.scaleBand()
      .domain(data.map((d, i) => d.name))
      .range([0, height])
      .paddingInner(0.3);

    return this;
  }

  renderAxes () {
    const { main, xScale, yScale, width, height } = this;

    const xAxisElement = main
      .select('.xAxis');
    const yAxisElement = main
      .select('.yAxis');

    this.xAxis = d3.axisBottom(xScale);
    xAxisElement
      .attr('transform', () => `translate(0, ${height})`)
      .call(this.xAxis);

    this.yAxis = d3.axisLeft(yScale);
    yAxisElement
      .call(this.yAxis);

    return this;
  }

  render (data) {
    this.renderAxes();

    const { main, xScale, yScale } = this;

    const seriesElement = main
      .select('.series');

    const bars = seriesElement
      .selectAll('rect')
      .data(data, d => d.id);

    bars.enter()
      .append('rect')
      .attr('class', 'bar')
      .merge(bars)
      .attr('height', yScale.bandwidth())
      .attr('x', 0)
      .attr('y', (d, i) => yScale(d.name))
      .transition()
      .duration(1000)
      .attr('width', d => xScale(d.value));

    bars.exit().remove();

    return this;
  }
}

class LineChart extends BaseChart {
  constructor (container) {
    super(container, 'line-chart');
  }

  setScales (data) {
    const { width, height } = this;

    this.xScale = d3.scaleTime()
      .domain(d3.extent(data, d => d.time))
      .range([0, width]);

    this.yScale = d3.scaleLinear()
      .domain(d3.extent(data, d => d.value))
      .range([height, 0]);

    return this;
  }

  renderAxes () {
    const { main, xScale, yScale, width, height } = this;

    const xAxisElement = main
      .select('.xAxis');
    const yAxisElement = main
      .select('.yAxis');

    this.xAxis = d3.axisBottom(xScale);
    xAxisElement
      .attr('transform', () => `translate(0, ${height})`)
      .call(this.xAxis);

    this.yAxis = d3.axisLeft(yScale);
    yAxisElement
      .call(this.yAxis);

    return this;
  }

  render (data) {
    this.renderAxes();

    const { main, xScale, yScale } = this;

    const seriesElement = main
      .select('.series');

    const lineGenerator = d3.line()
      .x(d => xScale(d.time))
      .y(d => yScale(d.value));

    const path = seriesElement
      .selectAll('path')
      .data([data]);

    path.enter()
      .append('path')
      .attr('class', 'line')
      .merge(path)
      .transition()
      //.duration(1000)
      .attr('d', lineGenerator);

    path.exit().remove();

    return this;
  }

  addZoomBehaviour () {
    const { main, width, height } = this;
    const { xAxis, yAxis, xScale, yScale } = this;

    const zoomElement = main
      .append('rect')
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      .attr('width', width)
      .attr('height', height);

    const zoom = d3.zoom()
      .scaleExtent([1, 100])
      .translateExtent([
        [-width / 2, -height / 2],
        [width * 1.5, height * 1.5],
      ])
      .on('zoom', () => {
        main
          .select('.series')
          .attr('transform', d3.event.transform);

        const newXScale = d3.event.transform.rescaleX(xScale);
        main
          .select('.xAxis')
          .call(xAxis.scale(newXScale));

        main
          .select('.yAxis')
          .call(yAxis.scale(d3.event.transform.rescaleY(yScale)));
      });

    zoomElement
      .call(zoom);

    return this;
  }

  updateXDomain (domain) {
    this.xScale.domain(domain);
  }
}

class ScatterChart extends BaseChart {
  constructor (container) {
    super(container, 'scatter-chart');
  }

  setScales (data) {
    const { width, height } = this;

    const xDomainMax = d3.max(data, d => d.value);
    this.xScale = d3.scaleLinear()
      .domain([0, xDomainMax * 1.1])
      .range([0, width]);

    const yDomainMax = d3.max(data, d => d.value2);
    this.yScale = d3.scaleLinear()
      .domain([0, yDomainMax * 1.1])
      .range([height, 0]);

    return this;
  }

  renderAxes () {
    const { main, xScale, yScale, width, height } = this;

    const xAxisElement = main
      .select('.xAxis');
    const yAxisElement = main
      .select('.yAxis');

    this.xAxis = d3.axisBottom(xScale);

    xAxisElement
      .attr('transform', () => `translate(0, ${height})`)
      .call(this.xAxis);

    this.yAxis = d3.axisLeft(yScale);

    yAxisElement
      .call(this.yAxis);

    return this;
  }

  //  data - data to be used to draw line chart (data structure shown above)
  render (data) {
    this.renderAxes();

    const { main, xScale, yScale } = this;

    const seriesElement = main
      .select('.series');

    const points = seriesElement
      .selectAll('circle')
      .data(data, (d) => d.time.getTime());

    points.enter()
      .append('circle')
      .attr('class', 'point')
      .merge(points)
      .attr('r', 2)
      .transition()
      .duration(1000)
      .attr('cx', d => xScale(d.value))
      .attr('cy', d => yScale(d.value2));

    points.exit().remove();

    return this;
  }

  addZoomBehaviour () {
    const { main, width, height } = this;
    const { xAxis, yAxis, xScale, yScale } = this;

    const zoomElement = main
      .append('rect')
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      .attr('width', width)
      .attr('height', height);

    const zoom = d3.zoom()
      .scaleExtent([1, 100])
      .translateExtent([
        [-width / 2, -height / 2],
        [width * 1.5, height * 1.5],
      ])
      .on('zoom', () => {
        main
          .select('.series')
          .attr('transform', d3.event.transform);

        main
          .select('.xAxis')
          .call(xAxis.scale(d3.event.transform.rescaleX(xScale)));

        main
          .select('.yAxis')
          .call(yAxis.scale(d3.event.transform.rescaleY(yScale)));
      });

    zoomElement
      .call(zoom);

    return this;
  }
}

//index.js
// barTimeChart
//   .setScales(values[0])
//   .render(values[0])
//   .addBrushBehaviour(function (newDomain) {
//     lineChart1.updateXDomain(newDomain);
//     lineChart2.updateXDomain(newDomain);
//
//     lineChart1.render(values[0]);
//     lineChart2.render(values[1]);
//     scatterChart
//       .render(values[1].filter(d => d.time >= newDomain[0] && d.time <= newDomain[1]));
//   });
