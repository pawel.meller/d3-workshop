import * as d3 from 'd3';
import { BaseChart } from './baseChart';

/* Data structure:
  [
    { value: Number, value2: Number },
    { value: Number, value2: Number },
    { value: Number, value2: Number },
    ...
  ]
* */



/* ----------- TASKS: ---------------

  1. Implement setScales  and render functions to draw scatter chart.
    Use d3.scaleLinear() for x scale.
    Use d3.scaleLinear() for y scale.
    This function will be called dynamically so it must implement enter/update/exit scenarios.

  2. Implement renderXAxis and renderYAxis functions to add axes to the chart.

  3*. Add zoom behaviour to the chart
    Add invisible (fill:none) rect to cover entire chart area (it will be zooming event "catcher"). Set its
     'pointer-events' to 'all' to catch all events.
    Use d3.zoom() to create zoom behaviour.

  Time: 15 min

  -------------------------------- */



export class ScatterChart extends BaseChart {
  constructor (container) {
    super(container, 'scatter-chart');
  }

  //  data - data to be used to draw line chart (data structure shown above)
  setScales (data) {
    const { width, height } = this;

    /*
        ------ YOUR CODE HERE (TASK #1) ------

        Note: Store both scales in "this", as they will be needed in other class methods.


        this.xScale = d3.scaleLinear() ...
        this.yScale = d3.scaleLinear() ...

    */

    return this;
  }

  renderAxes () {
    const { main, xScale, yScale, width, height } = this;

    const xAxisElement = main
      .select('.xAxis');

    const yAxisElement = main
      .select('.yAxis');
    /*
        ------ YOUR CODE HERE (TASK #2) ------

        Note: Store both scales in "this", as they will be needed in other class methods.

        Hint: Follow the same steps as in lineChart.

        this.xAxis = d3.axisBottom(xScale);
        xAxisElement...

        this.yAxis = d3.axisLeft(yScale);
        yAxisElement...

    */

    return this;
  }

  //  data - data to be used to draw scatter chart (data structure shown above)
  render (data) {
    this.renderAxes();

    const { main, xScale, yScale } = this;

    const seriesElement = main
      .select('.series');

    /*
        ------ YOUR CODE HERE (TASK #1) ------

        Note: Draw chart within seriesElement provided above.

        Hint: Use svg circles to draw scatter points. Then use class .point for them.
        Hint: Use time as data key.


        const points = seriesElement
          .selectAll(...)
          .data(...);

        points
          .enter()
          ...
          .merge(...)
          ...

    */

    return this;
  }

  addZoomBehaviour () {
    const { main, width, height } = this;
    const { xAxis, yAxis, xScale, yScale } = this;
    /*
        ------ YOUR CODE HERE (TASK #3) ------

        Hint: Follow the same steps as in lineChart.



        const zoomElement = main
          .append('rect')
          ...

        const zoom = d3.zoom()
          .scaleExtent(...)
          .translateExtent(...)
          .on('zoom', () => {
            ...
          });

        zoomElement...

        */

    return this;
  }
}
