/* eslint semi: ["error", "always"] */
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');

const lessons = {
  1: '01-dom-manipulation',
  2: '02-svg-shapes',
  3: '03-charts',
  4: '04-geo',
};

function getLessonPath (lessonNumber, part) {
  const directory = lessons[lessonNumber] || lessons[lessons.keys()[0]];
  return path.resolve(__dirname, directory, part ? `${part}.js` : `index.js`);
}

module.exports = function (env) {
  const entry = [
    require.resolve('react-dev-utils/webpackHotDevClient'),
    getLessonPath(env.lesson, env.part),
  ];

  return {
    entry,
    devtool: 'eval-source-map',
    resolve: {
      alias: {
        'react-color': path.resolve(__dirname, './src/index.js'),
        'test': path.resolve(__dirname, './03-charts/data.js'),
      },
      extensions: ['.js', '.sass'],
    },
    module: {
      rules: [
        {
          test: /\.(js)$/,
          use: 'eslint-loader',
          enforce: 'pre',
        },
        {
          test: /\.(js)$/,
          use: 'babel-loader',
          exclude: /node_modules/,
        },
        {
          test: /\.sass$/,
          use: [ 'style-loader', 'css-loader', 'sass-loader' ],
        },
        {
          test: /\.csv$/,
          loader: 'csv-loader',
          options: {
            dynamicTyping: true,
            header: true,
            skipEmptyLines: true,
          },
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'index.html',
      }),
      new webpack.DefinePlugin({
        LESSON: JSON.stringify(env.lesson),
        PART: JSON.stringify(env.part),
      }),
    ],
    devServer: {
      overlay: false,
      port: 9090,
      setup (app) {
        app.use(errorOverlayMiddleware());
      },
    },
  };
};
