import * as d3 from 'd3';
import './gol/gol.sass';
import { gol_svgSize, gol_cellsNumber } from './config';
import { generateRandomCells, generateNextCells } from './gol/algorithm';
import render from './lessonModules/gol-render';
const { requestAnimationFrame } = window;

const lessonTitle = 'D3.js - Lesson 2: Game of Life';

/**
 Render main DOM content.
 */
const main = d3.select('body')
  .append('div')
  .attr('class', 'main');

main.append('h1')
  .classed('title', true)
  .text(lessonTitle);

const buttons = main.append('div')
  .attr('class', 'buttons');

buttons.append('button')
  .text('Restart')
  .on('click', () => { gameStopped = false; start(); });

buttons.append('button')
  .text('Stop')
  .on('click', () => { gameStopped = true; });

const container = main
  .append('div')
  .attr('class', 'main__gol')
  .style('width', `${gol_svgSize}px`)
  .style('height', `${gol_svgSize}px`)
  .append('svg')
  .attr('width', `${gol_svgSize}px`)
  .attr('height', `${gol_svgSize}px`);

/**
 *
 * Game of life iterations.
 *
 * */
let gameStopped = false;

function start () {
  const cells = generateRandomCells(gol_cellsNumber);

  render(container, cells);

  requestAnimationFrame(iterate.bind(null, cells));
}

function iterate (prevCells) {
  const cells = generateNextCells(prevCells);

  render(container, cells);

  if (!gameStopped) {
    requestAnimationFrame(iterate.bind(null, cells));
  }
}

start();

