import * as d3 from 'd3';
import { gol_svgSize, gol_cellsNumber } from '../config';

/* Data structure
Cells data SAMPLE - in game this will be 100x100 array.
  [
    [0, 0, 1, 1, 1],
    [1, 0, 0, 0, 0],
    [0, 1, 1, 1, 0],
    [0, 1, 1, 0, 0],
    [0, 1, 0, 1, 1],
  ]
* */



/* ----------- TASKS: ---------------

  1. Implement render function to draw current state of all cells.
    Use svg circles.
    Cell's state (dead/ alive) should be represented by class on element (use .circle--alive class).

    This function will be called dynamically so it must implement enter/update scenarios. Exit is not required.

    SAMPLE svg circle: <circle cx="..." cy="..." r="..." stroke="..." fill="..." stroke-width="..."/>

  Time: 20 min

  -------------------------------- */

export default function render (elem, data) {
  const scale = d3.scaleLinear()
    .domain([0, gol_cellsNumber])
    .range([0, gol_svgSize]);

  const radius = gol_svgSize / gol_cellsNumber / 2;

  /*
        ------ YOUR CODE HERE (TASK #1) ------

        Use g element for 1st dimension of an array, and circle for 2nd dimension

        Hint: Merge entered and updated g elements to propagate 2nd dimension data to create circles:

          ` mergedGElements.selectAll('circle').data(function (d) { return d; } ); `

            Then for entered and updated circle elements set its attributes:
            ` .attr('cx', d => ...   .attr('cy', d => ...  .attr('r', d => ... `

            Set circle's class ("circle--alive") to represent the state of cell:
            ` .classed('circle--alive', d =>  ... `


        Hint: In data function, instead of passing nested array as-is, you can map it and add other stuff:

          ` .data(function(d, i) { return d.map((item, j) => ({ ... })); } ) `

          You can use this to create objects with x,y coordinates that will be set from parent-child array indexes


        Use pre-prepared scale function to determine x,y coordinates basing on arrays indexes:
          xCoord = scale(d.x)
          yCoord = scale(d.y)


        const groups = elem.selectAll('g')
          .data(data);
  */
}


