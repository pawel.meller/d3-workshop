
/* ----------- TASKS: ---------------

  1. Implement rendering 'div' DOM elements joined to provided data
    - Apply class 'bar' with 'bar--entered' to entered bars.
    - Set its width and text basing on data
    - Emphasize long bars depending on data (ex. > 200). Use classes 'bar--red' and 'text--big'.
    - Change opacity for rows > 5th, for example to 0.5.

  2. Implement updating existing elements (to reflect its joined data changes), and removing elements
    that are not joined to any data any more.
    - Apply class 'bar' with 'bar--updated' to updated bars. (Make sure to remove 'bar--entered')
    - Add the same features to the bars as for entered bars

  2*. Use .merge() method to eliminate code duplication for enter and update sections.

  3. Add transitions for smooth bars changes.

  4. Introduce svg and change div.bars to svg.rects.
    - Go to a-start.js file to introduce svg.bars instead of div.bars. Use imported svgWidth and svgHeight to setup svg element.
    - Re-implement your render function to replace divs with svg.rects.
    - Use class "rect", with "rect--entered" / "rect--updated", "rect--red"

    SAMPLE svg rect: <rect x="..." y="..." width="..." height="..." stroke="..." fill="..." stroke-width="..."/>

  4* Add svg text elements to show current data value. Place it next to related rect on svg area;

    SAMPLE svg text: <text x="..." y="..." stroke="..." fill="..."> Some text </text>

  Time: 45 min

  -------------------------------- */



//  * main - d3 element to which new data-driven elements should be appended
//  * data - data to be used to "drive" new elements
//    [55, 670, 1003, 33, 77, 340, 80]
export default function render (main, data) {
  // JOIN DATA
  // Select DOM elements you want to join with data and "connect" data array to them.
  /*
        ------ YOUR CODE HERE (TASK #1) ------

        Hint: Call .selectAll(...).data(...) on element to "join data to DOM elements" contained in this element.
        Hint: This returns update selection (so in this case 'bars' will be assigned with update selection).

        const bars = main.selectAll(...)
          .data(...);

  */


  // ENTER
  // Create new elements, and set its attributes
  /*
        ------ YOUR CODE HERE (TASK #1) ------

        Hint: .enter() called on update selection returns enter selection.
              Each element in enter selection has "datum" (single data item connected to element).
        Hint: Class, attributes, style etc can be set as plain values or as functions:
               (d, i, nodes) => {},  d - datum, i - index, nodes - current el group.

        bars.enter()
          .append('div')
          .attr('class', 'bar')
          .style('width', ...)
          .text(...)
          ...

  */



  // UPDATE
  // Update attributes for existing elements as needed.
  /*
        ------ YOUR CODE HERE (TASK #2) ------

        Hint: You can set attributes etc directly on update selection to modify existing elements.
              Each element in update selection has "datum".

        bars
         .classed(...)
         .style(...)
         ...

  */

  // Hint: You can merge the entered selection with the update selection, and apply operations to both.
  //       Call .merge(...) on enter selection, with update selection as argument -> any further attr changes will
  //       be applied to both.
  //
  // Hint: Call .transition() on selection -> any further attr changes will be smoothly transited.
  //       After .transition() you can parametrize it by calling .delay(..), .duration(...) etc.
  //       It will work even for exit selection - element will be removed from DOM after transiting attr/style.



  // EXIT
  // Remove old elements as needed.
  //
  /*
        ------ YOUR CODE HERE (TASK #2) ------

        Hint: .exit() called on update selection returns exit selection
        Hint: Call remove() on exit selection to remove all elements returned by this selection

        bars.exit() ...

  */
}





// READ MORE:
//
// MODIFYING ELEMENTS: https://github.com/d3/d3-selection#modifying-elements
//
// DATA JOINS: https://bl.ocks.org/mbostock/3808221
//             https://bost.ocks.org/mike/join/
//             https://github.com/d3/d3-selection#joining-data
//             http://nishacodes.tumblr.com/post/75661656346/d3s-enter-update-exit-lifecycle-is-like-git
//
// APPLYING MANY ATTRIBUTES: There is d3 module: https://github.com/d3/d3-selection-multi, that adds multi-value syntax
//    to selections and transitions, allowing you to set multiple attributes, styles or properties simultaneously
//      ex: element.attrs({one: "a", two: "b" })
//    This module is not included in the default D3 bundle.



