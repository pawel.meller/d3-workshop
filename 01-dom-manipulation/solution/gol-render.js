import * as d3 from 'd3';

function render (elem, data, svgSize, cellsNumber) {
  const scale = d3.scaleLinear()
    .domain([0, cellsNumber])
    .range([0, svgSize]);

  const radius = svgSize / cellsNumber / 2;

  /*
        ------ YOUR CODE HERE (TASK #1) ------

        Hint: Use g element to wrap 1st dimension of an array

        Hint: To propagate hierarchical data further form 1st dimension elements:

          ` g.selectAll('circle').data(function (d) { return d; } ); `

              where g is merged selection of new and existing g elements

        Hint: Instead of passing nested array as-is, you can map it and add other stuff (like something from parent):

          ` .data(function (d, i) { return d.map((item, item_index) => ({ val: ..., x: ..., y: ... })); }) `

              where x and y will be used to determine circle position, and value to determine cell's state.


  */
  /**  TASK SOLUTION: */
  const groups = elem.selectAll('g')
    .data(data);

  const circles = groups.enter()
    .append('g')
    .merge(groups)
    .selectAll('circle')
    .data(function (d, i) { return d.map((dc, j) => ({ val: dc, x: j, y: i })); });

  circles.enter().append('circle')
    .attr('class', 'circle')
    .attr('cx', (d) => scale(d.x) + radius / 2 + 0.5)
    .attr('cy', (d) => scale(d.y) + radius / 2 + 0.5)
    .attr('r', d => radius)
    .merge(circles)
    .classed('circle--alive', d => d.val === 1);
}
