import * as d3 from 'd3';

function renderBars (main, data) {
  /**  TASK 1 SOLUTION (data join): */
  let bars = main.selectAll('div.bar')
    .data(data, (d, i) => i);

  /**  TASK 1 SOLUTION: */
  bars.enter()
    .append('div')
    .classed('bar bar--entered', true)
    .classed('bar--red', d => d > 200)
    .style('opacity', (d, i) => i > 4 ? '0.5' : '1')
    .style('width', d => `${d}px`)
    .classed('text--big', d => d > 200)
    .text(d => d);

  /**  TASK 1 SOLUTION with adding span : */
  const newBars = bars.enter()
    .append('div')
    .classed('bar bar--entered', true)
    .classed('bar--red', d => d > 200)
    .style('opacity', (d, i) => i > 4 ? '0.5' : '1')
    .style('width', d => `${d}px`);

  newBars
    .append('span')
    .attr('class', 'text')
    .classed('text--big', d => d > 200)
    .text(d => d);

  /**  TASK 1 SOLUTION with adding span using each */
  bars.each(function (d) {
    d3.select(this)
      .append('span')
      .attr('class', 'text')
      .classed('text--big', d > 200)
      .text(d => d);
  });

  /**  TASK 2 SOLUTION: */
  bars.enter()
    .append('div')
    .classed('bar bar--entered', true)
    .classed('bar--red', d => d > 200)
    .style('opacity', (d, i) => i > 4 ? '0.5' : '1')
    .style('width', d => `${d}px`)
    .classed('text--big', d => d > 200)
    .text(d => d);

  bars
    .classed('bar--updated', true)
    .classed('bar--entered', false)
    .classed('bar--red', d => d > 200)
    .style('opacity', (d, i) => i > 4 ? '0.5' : '1')
    .style('width', d => `${d}px`)
    .classed('text--big', d => d > 200)
    .text((d) => d);

  bars.exit()
    .remove();

  /**  TASK 2* + TASK 3 SOLUTION: */
  bars.enter()
    .append('div')
    .classed('bar bar--entered', true)
    .merge(bars)
    .classed('bar--red', d => d > 200)
    .classed('text--big', d => d > 200)
    .text(d => d)
    .transition()
    .style('opacity', (d, i) => i > 4 ? '0.5' : '1')
    .style('width', d => `${d}px`);

  bars
    .classed('bar--updated', true)
    .classed('bar--entered', false);

  bars.exit()
    // .transition()
    // .duration(500)
    // .style('opacity', '0')
    .remove();

  /**  TASK 4 AND 4* SOLUTION with svg and texts: */
  bars = main.selectAll('rect')
    .data(data, (d, i) => i);

  bars.enter()
    .append('rect')
    .classed('rect rect--entered', true)
    .attr('x', () => 0)
    .attr('y', (d, i) => i * 40)
    .merge(bars)
    .classed('rect--red', d => d > 200)
    .attr('height', d => 30)
    .transition()
    .style('opacity', (d, i) => i > 4 ? '0.5' : '1')
    .attr('width', d => `${d}px`);

  const texts = main.selectAll('text')
    .data(data, (d, i) => i);

  texts.enter()
    .append('text')
    .classed('text', true)
    .attr('y', (d, i) => (i * 40 + 15))
    .attr('dy', '0.33em')
    .merge(texts)
    .classed('text--big', d => d > 200)
    .transition()
    .attr('x', d => `${d + 10}`)
    .text(d => d);

  bars
    .classed('rect--updated', true)
    .classed('rect--entered', false);

  bars.exit()
    .transition()
    .duration(500)
    .style('opacity', '0')
    .remove();

  texts.exit()
    .transition()
    .duration(500)
    .style('opacity', '0')
    .remove();
}
