
import _ from 'lodash';

const prev = (i, max) => i > 0 ? i - 1 : max;
const next = (i, max) => i < max ? i + 1 : 0;
const isAlive = c => c === 1;

function assertCellNeighbours ({ x, y, cells, maxX, maxY }, predicate) {
  const prevX = prev(x, maxX);
  const nextX = next(x, maxX);
  const prevY = prev(y, maxY);
  const nextY = next(y, maxY);

  const neighbours = [
    cells[prevX][prevY],
    cells[prevX][y],
    cells[prevX][nextY],
    cells[x][prevY],
    cells[x][nextY],
    cells[nextX][prevY],
    cells[nextX][y],
    cells[nextX][nextY],
  ];

  return predicate(neighbours);
}

export function generateRandomCells (cellsNumber) {
  const newCells = [];

  for (let i = 0, len = cellsNumber; i < len; i++) {
    newCells.push(_.times(cellsNumber, () => Math.random() < 0.5 ? 1 : 0));
  }

  return newCells;
}

export function generateNextCells (cells) {
  const newCells = [];

  for (let x = 0, lenX = cells.length; x < lenX; x++) {
    newCells[x] = [];

    for (let y = 0, lenY = cells[x].length; y < lenY; y++) {
      const cell = {
        x, y, cells, maxX: lenX - 1, maxY: lenY - 1,
      };
      const state = cells[x][y];

      const newState = state === 1
        ? assertCellNeighbours(cell, neighbours => _.includes([2, 3], neighbours.filter(isAlive).length))
        : assertCellNeighbours(cell, neighbours => _.includes([3], neighbours.filter(isAlive).length));

      newCells[x][y] = newState ? 1 : 0;
    }
  }

  return newCells;
}
