import * as d3 from 'd3';
// Since d3 v4.0 which is modular, it is possible to import only the module(s) that you actually need
//import { select } from 'd3-selection';

import './lesson01.sass';
import { svgWidth, svgHeight } from './config';
import { data1, data2 } from './data';
import render from './lessonModules/bars-render';

const lessonTitle = 'D3.js - Lesson 1: DOM manipulation with data joins';

/**
  Render DOM elements with d3
*/

// 1. Get d3.selection via d3.select() or d3.selectAll()

// 2. Modify DOM for selection via .append(), .insert(), .remove()

// 3. "Selection context" is changed after calling select/append/insert.

// 4. Apply class, attributes, style etc on current selection via:
//   .classed(), .attr(), .style(), .text() .property()

// 5. "Break" chain to store current selection.

const main = d3
  .select('body')
  .append('div')
  .attr('class', 'main');

main
  .append('h1')
  .classed('title', true)
  .text(lessonTitle);

const buttons = main.append('div')
  .attr('class', 'buttons');

main
  .append('div')
  .attr('class', 'main__bars')
  .append('div')
  .attr('class', 'bars');

/**
  Data joins

 - dynamic data driven rendering of DOM elements
 - transitions

*/
const renderBars = function (data) {
  const barsElement = main.select('.bars');

  render(barsElement, data);
};

// Bind events that will trigger re-render with new data.
const addDataButton = function (buttonText, data) {
  buttons.append('button')
    .text(buttonText)
    .on('click', () => renderBars(data));
};

const addUpdateDataButton = function (buttonText) {
  buttons.append('button')
    .text(buttonText)
    .on('click', () => {
      const currentData = main.selectAll('.bar, .rect')
        .data();

      renderBars(currentData.map(d => d + 50));
    });
};

renderBars(data1);
addDataButton('Load 1st datasource', data1);
addDataButton('Load 2nd datasource', data2);
//.addDataButton('Load 2nd datasource', [37, 400, ...data1]);
addUpdateDataButton('Broaden bars');





