# 1 d3.js workshop

### By:
- Karolina Czerniawska
- Paweł Meller

### Repository:
- https://...

# 2 Agenda

- Make Data to Drive html Documents.
- Use fancy d3.js "features" that make creating complex svg paths easy.
- Have a cup of cafe (short break :) )
- Create fully customizable charts from scratch.
- Embrace d3.geo and create interactive maps in a few lines of code.


# 3 Useful links

- https://d3js.org/ - documentation
- https://github.com/d3/d3/blob/master/API.md - full API specification
- https://bl.ocks.org/ - plenty of d3 working examples
- https://bost.ocks.org/mike/ - Mike Bostock's (d3 author) blog

- http://d3indepth.com/ - tutorial
- https://www.dashingd3js.com/table-of-contents - tutorial

# 3 "jQuery like" DOM manipulation

- select, selectAll
- append, insert, remove
- classed, attr, style, property, values
- chaining and node context
- storing "current" selection
```
const main = d3
  .select('body')
  .append('div') // each append/select introduces new selection context that further methods are applied on
  .attr('class', 'main');

main
  .append('h1')
  .classed('title', true)
  .text(lessonTitle);

const buttons = main.append('div')
  .attr('class', 'buttons');

main
  .append('div')
  .attr('class', 'main__bars')
  .append('div')
  .attr('class', 'bars');
  
buttons.append('button')
  .text(buttonText)
  .on('click', () => renderBars(data));
```

# 4 Enter - exit - update (data joins)

- DRAWING with code samples

## 4.1 Full package
```
const bars = body.selectAll('div.bar')
    .data(data, (d, i) => i)
    .enter()
    .append('div')
    .attr('class', 'bar')
    .text(d => d);
```

```
bars.enter()
    .append('div')
    .classed('bar', true)
    .text(d => d);

  bars
    .text((d) => d);

  bars.exit()
    .remove();
```

```
bars.enter()
    .append('div')
    .classed('bar', true)
    .merge(bars)
    .text(d => d);

  bars.exit()
    .remove();
 ```

## 4.1 todo (data -> bars drawing)
```
[ 55, 670, 1003, 33, 77, 340, 80 ];
```
DRAWING

## 4.2 Table in d3
```
  const rows = tableElem.selectAll('tr')
    .data(data);

  const cells = rows.enter()
    .append('tr')
    .merge(rows)
    .selectAll('td')
    .data(function (d, i) { return d.map((dc, j) => ({ val: dc, x: j, y: i })); });

  cells.enter()
    .append('td')
    .merge(cells)
    .text(d => d.val);

```

## 4.2 todo (data -> gol drawing)
```
[
    [0, 0, 1, 1, 1],
    [1, 0, 0, 0, 0],
    [0, 1, 1, 1, 0],
    [0, 1, 1, 0, 0],
    [0, 1, 0, 1, 1],
  ]
```
DRAWING

# 5 d3 shapes

Why?
- it is easy to render svg circle or rect but not creating paths
```
<circle cx="..." cy="..." r="..." stroke="..." fill="..." stroke-width="..."/>
<rect x="..." y="..." width="..." height="..." stroke="..." fill="..." stroke-width="..."/>
<line x1="..." y2="..." x2="..." y2="..." stroke="..." fill="..." stroke-width="..."/>
```
```
<path d="............." >
```
- we can use some predefined data transformations to make it mre "renderable"

## 5.1 Path generators:
- d3.line, d3.radialLine, d3.area

```
  const lineGroup = svg.append('g')
    .attr('class', 'line-example');

  // d3.line()(data) returns ready to use path.d value
  const lineGenerator = d3.line()
    // If data items are simple 2-element arrays of coordinates ([x, y]), you can skip x, y functions
    .x(d => (d[0]))
    .y(d => (d[1]))    
    .curve(d3.curveCardinal); // set curves for line

  lineGroup
    .append('path')
    .datum(lineData)
    .attr('d', lineGenerator);
```

```
  lineGroup
    .append('path')
    .attr('d', lineGenerator(lineData));
```

```
  lineGroup
    .selectAll('path')
    .data([lineData])
    .enter()
    .append('path')
    .attr('d', lineGenerator);
```

- d3.arc with d3.centeroid
```
  const arcGenerator = d3.arc()
    .innerRadius(20)
    .outerRadius(100);
```

```
  ...
  const centroid = arcGenerator.centroid(d);
  el
    .attr('x', centroid[0])
    .attr('y', centroid[1])
    .attr('dy', '0.33em')
```

- d3.symbol (star, ...)

```
  d3.symbol()
    .size([1500])
    .type(d3.symbolCircle)
    
    d3.symbolCircle,
    d3.symbolCross,
    d3.symbolDiamond,
    d3.symbolSquare,
    d3.symbolStar,
    d3.symbolTriangle,
    d3.symbolWye

```

## 5.1 todo: simple shapes
```
const lineData = [
    [0, 80],
    [10, 100],
    [20, 30],
    [30, 50],
    [40, 40],
    [50, 80],
  ];
```
DRAWING

```
?
```
DRAWING

```
const arcData = [
    { name: 'One', startAngle: 0, endAngle: 1 },
    { name: 'Two', startAngle: 1, endAngle: 1.9 },
    { name: 'Three', startAngle: 1.9, endAngle: 4 },
  ];
```
DRAWING

## 5.2 Data transformations:
- d3.stack, d3.pie
```
Fruits data:
  [
    {day: 'Mon', apricots: 120, blueberries: 180, cherries: 150},
    {day: 'Tue', apricots: 60, blueberries: 185, cherries: 105},
    {day: 'Wed', apricots: 100, blueberries: 215, cherries: 110},
    {day: 'Thu', apricots: 80, blueberries: 230, cherries: 55},
    {day: 'Fri', apricots: 120, blueberries: 240, cherries: 25},
  ]
```
```
  const stackGenerator = d3.stack()
    .keys(['apricots', 'blueberries', 'cherries']);

  const stackedSeriesData = stackGenerator(data);
```
```
 stackedSeriesData output:
   [
     [ [0, 120],   [0, 60],   [0, 100],    [0, 80],    [0, 120] ],   // Apricots
     [ [120, 300], [60, 245], [100, 315],  [80, 310],  [120, 360] ], // Blueberries
     [ [300, 450], [245, 350], [315, 425], [310, 365], [360, 385] ]  // Cherries
   ]
```

.

```
Fruits data:
  [
    {day: 'Mon', apricots: 120, blueberries: 180, cherries: 150},
    {day: 'Tue', apricots: 60, blueberries: 185, cherries: 105},
    ...
  ]
```
```
const pieGenerator = d3.pie()
    .value(d => d.apricots)
    .sort(null);

  const pieSeriesData = pieGenerator(data);
```
```
  pieSeriesData output:
  [
   {data: {day: "Mon", apricots: 120, blueberries: 180, cherries: 150}, index: 0, value: 120, startAngle: 0, endAngle: 1.570}
   {data: {day: "Tue", apricots: 60, blueberries: 185, cherries: 105}, index: 1, value: 60, startAngle: 1.570, endAngle: 2.356}
   ...
  ]
```

# 5.2 todo: stacked shapes (fruits charts)
```
Fruits data:
  [
    {day: 'Mon', apricots: 120, blueberries: 180, cherries: 150},
    {day: 'Tue', apricots: 60, blueberries: 185, cherries: 105},
    {day: 'Wed', apricots: 100, blueberries: 215, cherries: 110},
    {day: 'Thu', apricots: 80, blueberries: 230, cherries: 55},
    {day: 'Fri', apricots: 120, blueberries: 240, cherries: 25},
  ]
```
DRAWING

# 6 Charts

# d3.scales
```
 const scale = d3.scaleLinear()
   .domain([min, max]) // d3.min(array, accessor), d3.max(array, accessor)
   .range([0, width]);

 const scale = d3.scaleLinear()
   .domain(d3.extent(lineData, d => d.someValue))
   .range([0, width]);
```
```
  Domain: 0...100
  Range: 10...20

  scale(0) -> 10
  scale(50) -> 15

  scale.invert(15) -> 50
```

.

```
  const scale = d3.scaleBand()
      .domain(data.map((d, i) => d.name))
      .range([0, height])
      .paddingInner(0.5);
```
```
  Domain: Anna, Marta, Tomasz, Wojtek, Radek
  Range: 0...12

  scale("Marta") -> 2.5 (start point of the band)
  scale.bandwidth() -> 2 (size of "band" dependent on bands count and available space)

  scale.invert(3) -> "Marta"
```

.

```
  const scale = d3.scaleTime()
      .domain(minDate, maxDate)
      .range([0, 100]);
```
```
  Domain: new Date('2011-01-01') ... new Date('2011-01-10')
  Range: 0...100

  scale(Date('2011-01-01')) -> 0
  scale(Date('2011-01-05')) -> 50

  scale.invert(50) -> Date('2011-01-05')
```

# d3.axis

```
    const xAxisElement = d3.select('.xAxis');
    const yAxisElement = d3.select('.yAxis');

    const xAxis = d3.axisBottom(xScale);
    xAxisElement
      .attr('transform', () => `translate(0, ${chartHeight})`)
      .call(xAxis);

    const yAxis = d3.axisLeft(yScale);
    yAxisElement
      .call(yAxis);
```

# d3.zoom

```
   const zoomElement = main
      .append('rect')
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      .attr('width', width)
      .attr('height', height);

    const zoom = d3.zoom()

      .scaleExtent([1, 100])
      .translateExtent([
        [-width / 2, -height / 2],
        [width * 1.5, height * 1.5],
      ])
      .on('zoom', () => {
        main
          .select('.series')
          .attr('transform', d3.event.transform);

        const newXScale = d3.event.transform.rescaleX(xScale);
        main
          .select('.xAxis')
          .call(xAxis.scale(newXScale));

        main
          .select('.yAxis')
          .call(yAxis.scale(d3.event.transform.rescaleY(yScale)));
      });

    zoomElement
      .call(zoom);
```

# 6.1 todo: charts dashboard
DRAWING

# 7 d3.geo
