# d3.js workshop

This is introductory course that will guide you through basic concepts and features of d3.js library.

You will :
- learn how to make Data Drive your html Documents.
- see how to create your own, fully customizable charts.
- try out some d3 features that make creating complex svg shapes easy.
- get familiar with "d3 geo" and create maps in a few lines of code.


## Workshop authors
- Karolina Czerniawska
- Paweł Meller

## Useful links

- https://d3js.org/ - documentation
- https://github.com/d3/d3/blob/master/API.md - full API specification
- https://bl.ocks.org/ - plenty of d3 working examples
- https://bost.ocks.org/mike/ - Mike Bostock's (d3 author) blog

- http://d3indepth.com/ - tutorial
- https://www.dashingd3js.com/table-of-contents - tutorial
