/* eslint no-undef: "warn" */

// 1. drawEquatorconst
const equatorGeoLine = {
  type: 'LineString',
  coordinates: [[-180 + 1e-6, 0], [0, 0], [180, 0]],
};

function drawEquator () {
  svg.append('path')
    .classed('equator', true)
    .attr('d', path(equatorGeoLine));
};

// 2. ortographic projection
const projectionOrtographic = d3.geoOrthographic()
  .translate([WIDTH / 2, HEIGHT / 2])
  .rotate([-20, -30]);
