import * as d3 from 'd3';
import * as topojson from 'topojson';
import worldTopoJSON from 'world-atlas/world/50m.json';

import { WIDTH, HEIGHT } from './config';
import './styles.sass';

const projectionOrtographic = d3.geoOrthographic()
  .translate([WIDTH / 2, HEIGHT / 2])
  .rotate([-20, -30]);

const svg = d3.select('body').append('svg')
  .attr('width', WIDTH)
  .attr('height', HEIGHT);

const path = d3.geoPath()
  .projection(projectionOrtographic);

function drawGraticule () {
  const graticule = d3.geoGraticule();

  svg.append('path')
    .classed('graticule', true)
    .attr('d', path(graticule()));
}

drawGraticule();

/* EXERCISE 2a

  1) Read about recommended map projections for Europe (page 16, figure 1 and text below)
     http://www.ec-gis.org/sdi/publist/pdfs/annoni-etal2003eur.pdf

  2) Draw red bounding box surrounding Europe (implement `drawEuropeBoundingBox` method)
     using `europeGeoPolygon` and coordinates from PDF
*/

function drawCountries () {
  const worldGeoJSON = topojson.feature(worldTopoJSON, worldTopoJSON.objects.countries);

  svg.append('path')
    .classed('country', true)
    .attr('d', path(worldGeoJSON));
}

drawCountries();

const [N, S, W, E] = [71, 27, -32, 45];

const europeGeoPolygon = {
  type: 'Polygon',
  // NOTE: order of coordinates does matter!
  // Visualisation is worth a thousad words, so just go to
  // https://bl.ocks.org/mbostock/a7bdfeb041e850799a8d3dce4d8c50c8
  coordinates: [
    [/* TODO fill me */],
  ],
};

function drawEuropeBoundingBox () {
  // TODO implement
}

const projectionEurope = d3.geoConicConformal()
  .fitSize([WIDTH, HEIGHT], europeGeoPolygon)
  .rotate([-5, 0, 0]);

drawEuropeBoundingBox();

/* EXERCISE 2b

  Apply Conic Conformal projection to focus map on European area
  HINT: use result of previous task and documentation
  https://github.com/d3/d3-geo/blob/master/README.md#projection_fitSize
*/

/*
  READ MORE:

  How to choose proper map projection:
  * http://www.georeference.org/doc/guide_to_selecting_map_projections.htm
  * http://www.geo.hunter.cuny.edu/~jochen/gtech201/lectures/lec6concepts/map%20coordinate%20systems/how%20to%20choose%20a%20projection.htm

  Composite projections: http://geoexamples.com/d3-composite-projections/

  Custom projections: https://bl.ocks.org/mbostock/c7e85d2b47d11982db38

  Misc:
  * https://en.wikipedia.org/wiki/Web_Mercator
  * https://xkcd.com/977/
*/
