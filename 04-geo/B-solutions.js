/* eslint no-undef: "warn" */

// 2. Europe bounding box
const [N, S, W, E] = [71, 27, -32, 45];

const europeGeoPolygon = {
  type: 'Polygon',
  coordinates: [
    [[W, N], [E, N], [E, S], [W, S], [W, N]],
  ],
};

svg.append('path')
  .classed('europe-bbox', true)
  .attr('d', path(europeGeoPolygon));

// 3. projection focused on Europe
const projectionEurope = d3.geoConicConformal()
  .fitSize([WIDTH, HEIGHT], europeGeoPolygon)
  .rotate([-5, 0, 0]);
