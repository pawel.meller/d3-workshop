import * as d3 from 'd3';
import * as topojson from 'topojson';

import europeTopoJSON from './data/europe.topo.json';
import voivodeshipBirthrate from './data/voivodeship-birthrate.csv';
import { WIDTH, HEIGHT } from './config';
import './styles.sass';

// In previous part we used `world-atlas` NPM package to draw a map
// Let's analyze how this TopoJSON is created:
// https://github.com/topojson/world-atlas/blob/master/prepublish

// The following map uses TopoJSON created with `data/gen-europ-map.sh` script

// projection borrowed from https://github.com/rveciana/d3-composite-projections/blob/master/src/conicConformalEurope.js
export const projectionEurope = d3.geoConicConformal()
  .translate([WIDTH / 2, HEIGHT / 2])
  .scale(900)
  .rotate([-10, -53])
  .parallels([0, 60]);

const svg = d3.select('body').append('svg')
  .attr('width', WIDTH)
  .attr('height', HEIGHT);

const path = d3.geoPath()
  .projection(projectionEurope);

function drawGraticule () {
  const graticule = d3.geoGraticule();

  svg.append('path')
    .classed('graticule', true)
    .attr('d', path(graticule()));
}

drawGraticule();

function drawEurope () {
  const europeGeoJSON = topojson.feature(europeTopoJSON, europeTopoJSON.objects.countries);

  const countries = svg.selectAll('path')
    .data(europeGeoJSON.features)
    .enter().append('path')
    .attr('class', d => d.properties.continent === 'Europe' ? 'european-country' : 'country')
    .attr('d', path);
}

drawEurope();

/* EXERCISE 3

  1) Get administrative maps of Poland from http://biogeo.ucdavis.edu/data/diva/adm/POL_adm.zip
     and preprocess POL_adm1.shp (voivodeships) similarly like in data/gen-europe-map.sh

  2) Display preprocessed TopoJSON using d3, adjust projection to focus on Poland.

  3) Display data from `data/voivodeship-birthrate.csv` directly on map with color scale and text.
     (data was taken from :http://stat.gov.pl/statystyka-regionalna/rankingi-statystyczne/przyrost-naturalny-na-1000-ludnosci-wedlug-wojewodztw/)

  4) (optional) Add pan & zoom to map
     HINT: https://bl.ocks.org/mbostock/eec4a6cda2f573574a11
*/

/*
  READ MORE:

  "Let's make na map" tutorial:https://bost.ocks.org/mike/map/

  CLI cartography https://medium.com/@mbostock/command-line-cartography-part-1-897aa8f8ca2c

  Already projected topojson: https://bl.ocks.org/mbostock/5925375
*/
