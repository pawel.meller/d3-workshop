# To run `ogrinfo` and `ogr2ogr` CLI tools you need to install GDAL (Geospatial Data Abstraction Library)
# brew install gdal

# http://www.gdal.org/ogrinfo.html
# http://www.gdal.org/ogr2ogr.html

wget http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/cultural/ne_50m_admin_0_countries.zip
unzip ne_50m_admin_0_countries.zip

# get sample feature from shapefile (to see what properties are available)
ogrinfo -al -fid 1 ne_50m_admin_0_countries.shp -geom=NO

# display all possible subregions in Europer (Eastern Europe etc.)
ogrinfo ne_50m_admin_0_countries.shp -sql 'SELECT DISTINCT subregion FROM ne_50m_admin_0_countries WHERE continent="Europe"' -geom=NO

# clip geomeries to provided coordinates and output in GeoJSON format
ogr2ogr europe.geo.json ne_50m_admin_0_countries.shp -f "GeoJSON" -clipsrc -32 27 80 82

# convert GeoJSON to TopoJSON
../../node_modules/topojson-server/bin/geo2topo countries=europe.geo.json -o europe.topo.json -q 1e5

# compare filesizes of produced .json files
ls -lh *.json
