import * as d3 from 'd3';

import { WIDTH, HEIGHT } from './config';
import './styles.sass';

const projectionIdentity = d3.geoIdentity()
  .translate([WIDTH / 2, HEIGHT / 2])
  .scale(2);
  // it is also possible to draw on canvas element
  // .context(document.querySelect('canvas').getContext('2d'))
  // see: https://bl.ocks.org/mbostock/1de35c45f8718c678712

// create SVG root
const svg = d3.select('body').append('svg')
  .attr('width', WIDTH)
  .attr('height', HEIGHT);

// d3.geoPath() creates function that
// maps GeoJSON features to SVG paths
// using provided projection
const path = d3.geoPath()
  .projection(projectionIdentity);

// graticule - a network of lines representing meridians and parallels
function drawGraticule () {
  const graticule = d3.geoGraticule();

  svg.append('path')
    .classed('graticule', true)
    .attr('d', path(graticule()));
}

drawGraticule();

/* EXERCISE 1a

  Implement `drawEquator` method using `equatorGeoJson` geometry
*/

const equatorGeoLine = {
  type: 'LineString',
  coordinates: [/* TODO fill me */],
};

function drawEquator () {
  // TODO implement
};

drawEquator();

/* EXERCISE 1b

  Apply orthographic (`d3.geoOrthographic`) projection to the map
  https://github.com/d3/d3-geo/blob/master/README.md#geoOrthographic

*/

/*
  READ MORE:

  GeoJSON: https://en.wikipedia.org/wiki/GeoJSON

  d3-geo docs: https://github.com/d3/d3-geo
*/
